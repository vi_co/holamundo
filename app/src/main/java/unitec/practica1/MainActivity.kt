package unitec.practica1

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log



class MainActivity : AppCompatActivity() {

    private val TAG = " Hi "

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }

    override fun onStart(){
        super.onStart()
        Log.i (TAG, "onCreate")
        Log.i(TAG, "onStart")
        Log.i(TAG, "onResumen")
        Log.i(TAG, "onPause")
        Log.i(TAG, "onStop")
        Log.i(TAG, "onDestroy")

    }
 }

